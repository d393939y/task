from django.contrib.auth.models import update_last_login
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.encoding import smart_str, force_bytes, DjangoUnicodeDecodeError
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from rest_framework import exceptions, serializers, status
from rest_framework.exceptions import APIException
from rest_framework_simplejwt.settings import api_settings

from .models import UserProfile
from .utils import get_tokens_for_user


class APIException(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = "Validation Error"
    default_code = "error"

    def __init__(self, detail=None, code=None, status_code=status.HTTP_400_BAD_REQUEST):
        self.status_code = status_code
        super().__init__(detail, code)


class ResetPasswordSerializer(serializers.Serializer):
    """
    Serializer for Resetting Password
    """

    email = serializers.EmailField(required=False)
    new_password = serializers.CharField(required=False)


class TokenObtainPairSerializer(serializers.Serializer):
    email = serializers.EmailField(required=False)
    password = serializers.CharField()

    def validate(self, data):
        email = data.get("email")
        if not email:
            raise serializers.ValidationError("Email is required.")

        try:
            user = UserProfile.objects.get(email=email)
        except UserProfile.DoesNotExist:
            raise APIException(
                "UserProfile Does not exists", status_code=status.HTTP_404_NOT_FOUND
            )

        valid = user.check_password(data["password"])
        if valid:
            data = get_tokens_for_user(user)
            if api_settings.UPDATE_LAST_LOGIN:
                update_last_login(None, user)
        else:
            raise exceptions.AuthenticationFailed(
                "No active account found with the given credentials",
            )
        return data


class RegisterSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    phone = serializers.IntegerField(write_only=True)
    password = serializers.CharField(write_only=True)

    class Meta:
        model = UserProfile
        fields = ["email", "password", "name", "phone"]

    def validate(self, attrs):
        # checking is email exists
        email_exists = UserProfile.objects.filter(
            email=attrs["email"]
        ).exists()  # returns bool

        # checking is phone exists
        phone_exists = UserProfile.objects.filter(
            phone=attrs["phone"]
        ).exists()  # returns bool
        if email_exists or phone_exists:
            raise APIException("Details already exists")
        return super().validate(attrs)

    # we need to hash the user's passwords manually with this methord
    def create(self, validated_data):
        user = UserProfile.objects.create(**validated_data)
        user.set_password(validated_data["password"])
        user.save()
        return user


class ProfileSerializer(serializers.ModelSerializer):

    def validate(self, attrs):
        # checking is phone exists
        if attrs.get("phone"):
            phone_exists = UserProfile.objects.filter(
                phone=attrs["phone"]
            ).exists()  # returns bool
            if phone_exists:
                raise APIException("Phone already exists")
        return super().validate(attrs)

    class Meta:
        model = UserProfile
        fields = ["name", "phone"]


class ForgetPasswordEmailSerializer(serializers.Serializer):
    email = serializers.EmailField(max_length=255)

    class Meta:
        fields = ['email']

    def validate(self, attrs):
        email = attrs.get('email')
        if UserProfile.objects.filter(email=email).exists():
            user = UserProfile.objects.get(email=email)
            uid = urlsafe_base64_encode(force_bytes(user.id))
            print('Encoded UID', uid)
            token = PasswordResetTokenGenerator().make_token(user)
            print('Password Reset Token', token)
            link = 'http://localhost:8000/api/reset-password/' + uid + '/' + token + '/'
            print('Password Reset Link', link)
            # Send EMail
            body = 'Click Following Link to Reset Your Password ' + link
            data = {
                'subject': 'Reset Your Password',
                'body': body,
                'to_email': user.email
            }
            # utils.send_email(data)
            return attrs
        else:
            raise serializers.ValidationError('You are not a Registered User')


class UserPasswordResetSerializer(serializers.Serializer):
    password = serializers.CharField(max_length=255, style={'input_type': 'password'}, write_only=True)
    password2 = serializers.CharField(max_length=255, style={'input_type': 'password'}, write_only=True)

    class Meta:
        fields = ['password', 'password2']

    def validate(self, attrs):
        try:
            password = attrs.get('password')
            password2 = attrs.get('password2')
            uid = self.context.get('uid')
            token = self.context.get('token')
            if password != password2:
                raise serializers.ValidationError("Password and Confirm Password doesn't match")
            id = smart_str(urlsafe_base64_decode(uid))
            user = UserProfile.objects.get(id=id)
            if not PasswordResetTokenGenerator().check_token(user, token):
                raise serializers.ValidationError('Token is not Valid or Expired')
            user.set_password(password)
            user.save()
            return attrs
        except DjangoUnicodeDecodeError as identifier:
            PasswordResetTokenGenerator().check_token(user, token)
            raise serializers.ValidationError('Token is not Valid or Expired')


class GenerateOTPSerializer(serializers.Serializer):
    email = serializers.EmailField()

    class Meta:
        fields = ["email"]


class VerifyTokenSerializer(serializers.Serializer):
    email = serializers.EmailField(required=False)
    otp = serializers.IntegerField()

from django.urls import path

from api.user.views import TokenObtainPairView, RegisterView, ChangePasswordView, ProfileView, \
    ForgetPasswordEmailView, UserPasswordResetView, GenerateOTP, VerifyOTPAPIView

urlpatterns = [
    path("login/", TokenObtainPairView.as_view(), name="token_obtain_pair"),
    path("register/", RegisterView.as_view(), name="register"),
    path("change-password/", ChangePasswordView.as_view(), name="change_password"),

    path("update-profile/<int:pk>/", ProfileView.as_view(), name="update_profile"),
    path('send-reset-password-email/', ForgetPasswordEmailView.as_view(), name='send-reset-password-email'),
    path('reset-password/<uid>/<token>/', UserPasswordResetView.as_view(), name='reset-password'),

    path('generate-otp/', GenerateOTP.as_view(), name="generate_otp"),
    path('verify-otp/', VerifyOTPAPIView.as_view(), name="verify_otp"),
]

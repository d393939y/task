# Register your models here.

from django.contrib import admin

from api.user.models import UserProfile

admin.site.register(UserProfile)

import random
import string

from django.db.models.signals import pre_save
from django.dispatch import receiver

from api.user.models import UserProfile


def generate_otp_key():
    """
    User otp key generator
    """
    otp_key = "".join(
        random.choices(string.ascii_uppercase + string.digits, k=10)  # nosec
    )
    if UserProfile.objects.filter(otp_key=otp_key).exists():
        generate_otp_key()
    return otp_key


@receiver(pre_save, sender=UserProfile, dispatch_uid="create_key")
def create_key(sender, instance, **kwargs):
    """
    This creates the key for users that don't have keys
    """
    if not instance.otp_key:
        instance.otp_key = generate_otp_key()

from rest_framework import permissions


class ProfilePermission(permissions.BasePermission):
    """
    Object-level permission to allow only if user role has permission to access the API
    """

    def has_permission(self, request, view):
        # list, create
        if request.user.groups.filter(name="Admin").exists():
            return True

    def has_object_permission(self, request, view, obj):
        # retrive, put, patch, delete
        if request.user.groups.filter(name="Admin").exists():
            return True

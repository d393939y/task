# Generated by Django 3.2.15 on 2022-11-30 17:33

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('user', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userprofile',
            name='is_invited',
        ),
        migrations.RemoveField(
            model_name='userprofile',
            name='is_verified',
        ),
    ]

from django.contrib.auth.models import update_last_login
from rest_framework import exceptions
from rest_framework import status
from rest_framework.generics import CreateAPIView, UpdateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.settings import api_settings
from rest_framework_simplejwt.views import TokenObtainPairView
from spookyotp import TOTP

from api.user.permissions import ProfilePermission
from .models import UserProfile
from .serializers import TokenObtainPairSerializer, ResetPasswordSerializer, RegisterSerializer, ProfileSerializer, \
    ForgetPasswordEmailSerializer, UserPasswordResetSerializer, GenerateOTPSerializer, VerifyTokenSerializer
from .utils import APIException, get_response
from .utils import get_tokens_for_user


class TokenObtainPairView(TokenObtainPairView):
    """
    Takes a set of user credentials and returns an access and refresh JSON web
    token pair to prove the authentication of those credentials.
    """
    serializer_class = TokenObtainPairSerializer


class ChangePasswordView(APIView):
    """
    Generate OTP API
    """
    serializer_class = ResetPasswordSerializer
    permission_classes = [IsAuthenticated]

    def post(self, request):
        new_password = request.data.get("new_password", None)

        if not new_password:
            raise APIException("Please enter a new Password")

        request.user.set_password(new_password)
        request.user.get_t
        request.user.save()

        return Response(
            {"message": "Password reset successfully"},
            status.HTTP_200_OK,
        )


class RegisterView(CreateAPIView):
    serializer_class = RegisterSerializer
    model = UserProfile


class ProfileView(UpdateAPIView):
    serializer_class = ProfileSerializer
    model = UserProfile
    permission_classes = [IsAuthenticated, ProfilePermission]
    queryset = UserProfile.objects.all()
    lookup_field = 'pk'

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop("partial", False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        if getattr(instance, "_prefetched_objects_cache", None):
            instance._prefetched_objects_cache = {}
        return get_response(serializer.data, status_code=status.HTTP_200_OK)


class ForgetPasswordEmailView(APIView):
    def post(self, request):
        serializer = ForgetPasswordEmailSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response({'msg': 'Password Reset link send. Please check your Email'}, status=status.HTTP_200_OK)


class UserPasswordResetView(APIView):
    def post(self, request, uid, token):
        serializer = UserPasswordResetSerializer(data=request.data, context={'uid': uid, 'token': token})
        serializer.is_valid(raise_exception=True)
        return Response({'msg': 'Password Reset Successfully'}, status=status.HTTP_200_OK)


class GenerateOTP(CreateAPIView):
    serializer_class = GenerateOTPSerializer

    def post(self, request):
        serializer = GenerateOTPSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        email = serializer.data.get("email")

        try:
            instance = UserProfile.objects.get(email=email)
        except UserProfile.DoesNotExist:
            return Response({'msg': 'User Does not Exists'}, status=status.HTTP_404_NOT_FOUND)

        otp = TOTP(bytearray(instance.otp_key, "utf-8"), "Talpoint", period=120).get_otp()

        return Response({'msg': f'OTP send to email, this is dev server so your otp is: {otp}'},
                        status=status.HTTP_200_OK)


class VerifyOTPAPIView(CreateAPIView):
    serializer_class = VerifyTokenSerializer

    def post(self, request):
        serializer = VerifyTokenSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        data = serializer.data

        email = data.get("email")
        otp = data.get("otp")
        if not email:
            raise APIException("Email is required.")

        try:
            user = UserProfile.objects.get(email=email)
        except UserProfile.DoesNotExist:
            raise APIException(
                "UserProfile Does not exists", status_code=status.HTTP_404_NOT_FOUND
            )

        totp = TOTP(bytearray(user.otp_key, "utf-8"), "Test", period=120)
        if totp.compare(str(otp)):
            data = get_tokens_for_user(user)
            if api_settings.UPDATE_LAST_LOGIN:
                update_last_login(None, user)
        else:
            raise exceptions.AuthenticationFailed(
                "No active account found with the given credentials",
            )
        return Response(data)

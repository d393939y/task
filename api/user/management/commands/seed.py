from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand

User = get_user_model()


class Command(BaseCommand):
    def handle(self, *args, **options):
        try:
            self.create_groups()
            self.stdout.write(self.styel.SUCCESS("Seed completed successfully"))

        except Exception as ex:
            print("➡ ex :", ex)
            self.stdout.write(self.style.ERROR(f"{str(ex)}"))

    def create_groups(self):
        Group.objects.get_or_create(name="Admin")
        Group.objects.get_or_create(name="Solution Provider")
        Group.objects.get_or_create(name="Solution Seeker")
        self.stdout.write(self.style.SUCCESS("groups created successfully"))

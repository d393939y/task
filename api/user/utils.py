import jwt
from django.conf import settings
from django.db import models
from rest_framework import status
from rest_framework.exceptions import APIException
from rest_framework.response import Response
from rest_framework_simplejwt.tokens import RefreshToken


class APIException(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = "Validation Error"
    default_code = "error"

    def __init__(self, detail=None, code=None, status_code=status.HTTP_400_BAD_REQUEST):
        self.status_code = status_code
        super().__init__(detail, code)


def get_tokens_for_user(user):
    refresh = RefreshToken.for_user(user)

    data = {}
    data["refresh"] = str(refresh)
    access_token = str(refresh.access_token)
    decodeJTW = jwt.decode(str(access_token), settings.SECRET_KEY, algorithms=["HS256"])

    # add payload here!!
    decodeJTW["user_id"] = user.id
    decodeJTW["name"] = user.name
    decodeJTW["email"] = user.email
    encoded = jwt.encode(decodeJTW, settings.SECRET_KEY, algorithm="HS256")

    data["access"] = encoded
    return data


class Timestamp(models.Model):
    created_ts = models.DateTimeField(auto_now_add=True)
    modified_ts = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(
        "user.UserProfile", on_delete=models.SET_NULL, null=True
    )

    class Meta:
        abstract = True


def get_response(
        data={},
        message="",
        error="",
        error_list=[],
        status_code=status.HTTP_200_OK,
        headers={},
):
    response_data = {
        "data": data,
        "message": message,
        "error": error,
        "error_list": error_list,
        "status": status_code,
    }
    return Response(
        response_data,
        status=status_code,
        headers=headers,
    )


def get_access_token(user):
    data = {}
    refresh = RefreshToken.for_user(user)
    data["refresh"] = str(refresh)
    access_token = str(refresh.access_token)
    data["access"] = access_token
    return data
